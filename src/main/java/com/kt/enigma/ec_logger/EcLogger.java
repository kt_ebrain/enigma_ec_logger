package com.kt.enigma.ec_logger;


import com.kt.enigma.ec_logger.utils.ConsumerConfigs;
import com.kt.smcp.gw.ca.upsysfrwk.adap.m3.vo20.integration.avro.CollectEvent;
import kafka.consumer.Consumer;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.kt.smcp.gw.ca.upsysfrwk.adap.m3.vo20.integration.avro.CollectEvent;

@Component
public class EcLogger {

    private static final Logger logger = LoggerFactory.getLogger(EcLogger.class);


    private ConsumerConnector consumer;

    @Autowired
    ConsumerConfigs consumerConfigs;
    //kafka topic name
    @Value("${coolingcontrol.consumer.topic.name}")
    private String topicNm;


    public void run(String[] args) {

        try {

            logger.info("====================================================");
            logger.info("[RUNAPP]CoolingControlConsumerApp Start......");
            logger.info("====================================================");


            consumer = Consumer.createJavaConsumerConnector(consumerConfigs.getConsumerConfig());

            Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
            topicCountMap.put(topicNm, 1);

            Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
            List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topicNm);

            logger.info("Kafka Consumer Start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//            private Type type = new TypeToken<Map<String, Object>>(){}.getType();
//            private Map<String,Object> message = new HashMap<String,Object>();
//
//    		List<KafkaStream<byte[], byte[]>> streams = consumerStreams.get(topic);
//

            for (final KafkaStream<byte[], byte[]> stream : streams) {

                for (MessageAndMetadata<byte[], byte[]> messageAndMetadata : stream) {

                    try {

                        logger.info("--------------------------------------------------------------");

                        BinaryDecoder binaryDecoder = DecoderFactory.get().binaryDecoder(messageAndMetadata.message(), null);

                        SpecificDatumReader<CollectEvent> specificDatumReader = new SpecificDatumReader<CollectEvent>(CollectEvent.class);
                        CollectEvent event = new CollectEvent();
                        try {
                            event = specificDatumReader.read(null, binaryDecoder);
                        } catch (IOException e) {
//            				throw new Exception("Reading Error:"+event.toString());
                        }

                        logger.warn("[READ_STREAM] offset: {}, message => {}", messageAndMetadata.offset(), event);

                        if (event.getAttributes() != null)
//                            processEvent(event);

                        logger.info("--------------------------------------------------------------");

                    } catch (Exception e) {
                        logger.error("", e);
                    }
                }
            }
            logger.debug("[kafka Stream for-end]=============================================");

//            consumer.shutdown();

        } catch (Exception e) {
            logger.error("", e);
        } finally {

//            shutdownConsumerConnector();

            logger.info("====================================================");
            logger.info("[RUNAPP]CoolingControlConsumerApp End......");
            logger.info("====================================================");
        }

    }

}
