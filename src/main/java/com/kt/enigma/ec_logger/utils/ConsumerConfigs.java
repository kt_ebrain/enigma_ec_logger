package com.kt.enigma.ec_logger.utils;


import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import kafka.consumer.ConsumerConfig;

@Component
public class ConsumerConfigs {

    //ktmeg zookeeper URL
    @Value("${ktmeg.kafka.zk.url}")
    private String zkUrl;

    //consumer group id
    @Value("${coolingcontrol.consumer.group.id}")
    private String cosumerGroupId;

    public ConsumerConfig getConsumerConfig() {

        Properties props = new Properties();
        props.put("group.id", cosumerGroupId);
        props.put("zookeeper.connect", zkUrl);
//        props.put("auto.commit.enable", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("fetch.message.max.bytes", "10485760");
//        props.put("zookeeper.sync.time.ms", "2000");
//        props.put("auto.offset.reset", "latest");
        props.put("zookeeper.connection.timeout.ms", "30000");

        ConsumerConfig consumerConfig = new ConsumerConfig(props);
        return consumerConfig;
    }

}
