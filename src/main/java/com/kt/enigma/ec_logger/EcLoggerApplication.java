package com.kt.enigma.ec_logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class EcLoggerApplication {

	public static void main(String[] args) {
//		SpringApplication.run(EcLoggerApplication.class, args);
		SpringApplication app = new SpringApplication(EcLoggerApplication.class);

		ConfigurableApplicationContext ctx = app.run(args);
        ctx.getBean(EcLogger.class).run(null);

	}
}
